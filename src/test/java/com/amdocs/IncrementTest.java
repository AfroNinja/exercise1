package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testDecreasecounter() throws Exception {
        int k= new Increment().decreasecounter(0);
        assertEquals("Decreasecounter",0 ,k);        

        int a= new Increment().decreasecounter(1);
        assertEquals("Decreasecounter",1,a);

        int b= new Increment().decreasecounter(2);
        assertEquals("Decreasecounter",2,b);
    }


}

