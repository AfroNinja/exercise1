package com.amdocs;

public class Increment {
	
	private int counter = 1;
	  
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		if (input == 0){
	        	counter--;
		}
		else if (input != 1){
			counter++;
		}
		return counter;
		
	}			
}
